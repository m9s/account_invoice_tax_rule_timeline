#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import datetime
from trytond.model import ModelView, ModelSQL, fields


class RuleLine(ModelSQL, ModelView):
    _name = 'account.tax.rule.line'

    valid_from = fields.Date('Valid from')
    valid_to = fields.Date('Valid to')

    def match(self, line, pattern):
        '''
        Match line on pattern

        :param line: a BrowseRecord of rule line
        :param pattern: a dictonary with rule line field as key
                and match value as value
        :return: a boolean
        '''
        res = True
        for field in pattern.keys():
            if field == 'effective_date':
                if isinstance(pattern[field], datetime.datetime):
                    pattern[field] = pattern[field].date()
                if line['valid_from'] and line['valid_from'] > pattern[field]:
                    res = False
                    break
                if line['valid_to'] and line['valid_to'] < pattern[field]:
                    res = False
                    break
            if field not in self._columns:
                continue
            if not line[field] and field != 'group':
                continue
            if self._columns[field]._type == 'many2one':
                if line[field].id != pattern[field]:
                    res = False
                    break
            else:
                if line[field] != pattern[field]:
                    res = False
                    break
        return res

RuleLine()


class RuleLineTemplate(ModelSQL, ModelView):
    _name = 'account.tax.rule.line.template'

    valid_from = fields.Date('Valid from')
    valid_to = fields.Date('Valid to')

    def _get_tax_rule_line_value(self, template, rule_line=None):
        '''
        Set values for tax rule line creation.

        :param template: the BrowseRecord of the template
        :param rule_line: the BrowseRecord of the rule line to update
        :return: a dictionary with rule line fields as key and values as value
        '''
        res = super(RuleLineTemplate, self)._get_tax_rule_line_value(template)
        if not rule_line or rule_line.valid_from != template.valid_from:
            res['valid_from'] = template.valid_from
        if not rule_line or rule_line.valid_to != template.valid_to:
            res['valid_from'] = template.valid_to
        return res

RuleLineTemplate()
