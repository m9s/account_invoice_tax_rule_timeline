# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Tax Rule Timeline',
    'name_de_DE': 'Fakturierung Steuerregel Gültigkeitszeitraum',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Timeline for Tax Rules on Invoices
    - Adds validity dates to tax rules
    - Adds effective date to invoice
''',
    'description_de_DE': '''Gültigkeitszeitraum für Steuerregeln in Rechnungen
    - Fügt Datumsfelder für den Gültigkeitszeitraum von Steuerregeln hinzu
    - Fügt das Feld Effektives Datum zu Rechnungen hinzu
''',
    'depends': [
        'account_invoice'
    ],
    'xml': [
        'invoice.xml',
        'tax.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
