#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.pool import Pool


class Invoice(ModelSQL, ModelView):
    _name = 'account.invoice'

    effective_date = fields.Function(fields.Date('Effective Date'),
            'get_effective_date')

    def __init__(self):
        super(Invoice, self).__init__()

        attrs = {
            'on_change': [
                'effective_date',
                'accounting_date',
                'invoice_date'
                ],
            'depends': ['effective_date']
            }

        fields = {
            'invoice_date': attrs,
            'accounting_date': attrs,
            'party': attrs,
            }

        for field in fields:
            if not hasattr(self, field):
                continue
            setattr(self, field, copy.copy(getattr(self, field)))
            self_field = getattr(self, field)
            for attribute in fields[field]:
                if getattr(self_field, attribute) is None:
                    setattr(self_field, attribute, fields[field][attribute])
                    continue
                for value in fields[field][attribute]:
                    if value not in getattr(self_field, attribute):
                        new_value = getattr(self_field, attribute) + [value]
                        setattr(self_field, attribute, new_value)

        '''
        alternative: repeating but more readable

        self.invoice_date = copy.copy(self.invoice_date)
        if self.invoice_date.on_change is None:
            self.invoice_date.on_change = []
        for value in ['effective_date', 'accounting_date', 'invoice_date']:
            if value not in self.invoice_date.on_change:
                self.invoice_date.on_change += [value]
        if self.invoice_date.depends is None:
            self.invoice_date.depends = []
        if 'effective_date' not in self.invoice_date.depends:
            self.invoice_date.depends += ['effective_date']

        self.accounting_date = copy.copy(self.accounting_date)
        if self.accounting_date.on_change is None:
            self.accounting_date.on_change = []
        for value in ['effective_date', 'accounting_date', 'invoice_date']:
            if value not in self.accounting_date.on_change:
                self.accounting_date.on_change += [value]
        if self.accounting_date.depends is None:
            self.accounting_date.depends = []
        if 'effective_date' not in self.accounting_date.depends:
            self.accounting_date.depends += ['effective_date']
        '''

        self._rpc.update({
            'on_change_invoice_date': False,
            'on_change_accounting_date': False,
        })
        self._reset_columns()

    def default_effective_date(self):
        date_obj = Pool().get('ir.date')
        return date_obj.today()

    def on_change_invoice_date(self, vals):
        date_obj = Pool().get('ir.date')
        today = date_obj.today()

        res = {}
        if vals.get('accounting_date'):
            res['effective_date'] = vals.get('accounting_date')
        elif vals.get('invoice_date'):
            res['effective_date'] = vals.get('invoice_date')
        else:
            res['effective_date'] = today
        return res

    def on_change_accounting_date(self, vals):
        return self.on_change_invoice_date(vals)

    def on_change_party(self, vals):
        context = {}
        if vals.get('effective_date'):
            context['effective_date'] = vals['effective_date']
        with Transaction().set_context(**context):
            res = super(Invoice, self).on_change_party(vals)
        return res

    def get_effective_date(self, ids, name):
        date_obj = Pool().get('ir.date')
        today = date_obj.today()

        res = {}
        for invoice in self.browse(ids):
            if invoice.accounting_date:
                res[invoice.id] = invoice.accounting_date
            elif invoice.invoice_date:
                res[invoice.id] = invoice.invoice_date
            else:
                res[invoice.id] = today
        return res

Invoice()


class Line(ModelSQL, ModelView):
    _name = 'account.invoice.line'

    def __init__(self):
        super(Line, self).__init__()

        self.product = copy.copy(self.product)
        if self.product.on_change is None:
            self.product.on_change = []
        if '_parent_invoice.effective_date' not in self.product.on_change:
            self.product.on_change += ['_parent_invoice.effective_date']
        self._reset_columns()

    def _get_tax_rule_pattern(self, party, vals):
        '''
        Get tax rule pattern

        :param party: the BrowseRecord of the party
        :param vals: a dictionary with value from on_change
        :return: a dictionary to use as pattern for tax rule
        '''
        res = super(Line, self)._get_tax_rule_pattern(party, vals)
        if vals.get('_parent_invoice.effective_date'):
            res['effective_date'] = vals['_parent_invoice.effective_date']
        return res

    def on_change_product(self, vals):
        context = {}
        if vals.get('_parent_invoice.effective_date'):
            context['effective_date'] = vals['_parent_invoice.effective_date']
        with Transaction().set_context(**context):
            res = super(Line, self).on_change_product(vals)
        return res

Line()
